FROM ubuntu:xenial as builder

ENV GO_VERSION 1.6
ENV PATH $PATH:/usr/lib/go-$GO_VERSION/bin/go
ENV GOPATH $(go env GOPATH)

WORKDIR /go/src/github.com/aptible/supercronic/

# Install Go & Glide
RUN apt-get update && \
    apt-get install -y golang-$GO_VERSION-go software-properties-common git && \
    chmod +x /usr/lib/go-$GO_VERSION/bin/go && \
    add-apt-repository ppa:masterminds/glide && \
    apt-get update && \
    apt-get install -y glide

# Clone repository and build supercronic
RUN git clone https://github.com/aptible/supercronic.git . && \
    glide install && \
    make build

# Build final image
FROM ubuntu:xenial
WORKDIR /root/
COPY --from=builder /go/src/github.com/aptible/supercronic/ .
ENTRYPOINT ["./supercronic"]